import os
from pathlib import Path
import dlib
import argparse


TRAINED_DETECTOR_PATH = './models/detector.weights'
TRAINED_DETECTOR_WITH_AUGM_PATH = './models/detector_augmented.weights'

def main():

	# read images
	images = {}
	path = Path(args['path'])
	for f in sorted(path.rglob('*.png')):
	    print(f)
	    print("Adding image: {}".format(f))
	    images[f] = dlib.load_rgb_image(str(f.absolute()))

	# load detectors
	detectors = {
		'normal': dlib.simple_object_detector(TRAINED_DETECTOR_PATH),
		'augmented': dlib.simple_object_detector(TRAINED_DETECTOR_WITH_AUGM_PATH),
	}

	for name, detector in detectors.items():
		print('Detector: %s' % name)

		for img_name, img in images.items():
			dets = detector(img)
			print("Image: {} Number of dents detected: {}".format(img_name, len(dets)))

			print(args.keys())
			if args['show']:
				win = dlib.image_window()
				win.clear_overlay()
				win.set_image(img)
				win.add_overlay(dets)
				win.wait_until_closed()

if __name__ == "__main__":
	#parse arguments
	ap = argparse.ArgumentParser()
	ap.add_argument("-p","--path", required=True, help="path to images")
	ap.add_argument("-s","--show", action="store_true", help="show detected dents")
	args = vars(ap.parse_args())
    
	main()