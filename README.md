Dents that are highlighted by the linear lamps creates a non-linearities.
On three examples there are 2 types of dents: convex and concave
This nonlinearities is simple pattern on the image with light lines and we have not so much data to learn pattern, so the the best one solution here is HOG + SVM.
Oriented gradients can be perfectly trained to detect simple similar objects.
I will use this algorythm.

Other methods that can be used here:

- HOG+SVM
- Neural Networks like Yolo/SSD (light mobile versions).
They can be used but trained on low quantity dataset the will overfitt, also they training will require more time and processing will require much more resources.
Augmentations can expand dataset and filters will learn pattern and of course NN will work. But it's not optimized way.
- Tensorflow Conv Filters + Pyramydal Image scanning or Floating Window scanning.
Instead of learning the oriented gradients (HOG) we can simple create masks/filters for them. We can crop non-linearities and use it as True data and crop linearities and use them as False data. 
We add some augmentations before cropping to extend dataset a lot.
After that we can train Conv filters by minimizyng binary cross-entropy in predicted Classes with Tensorflow/Pytorch that will learn pattern of non-linearities (but not gradients).
Applying that filter with `sliding window` we can find places where dents are.
This method are very good too (as HOG+ SVM) and if optimize and parallelize computing on GPU it will be very fast.
And it can learn more hard dents with harder patterns  (but I have to see full dataset to understand more concretly).

- Some combination of classical CV methods (Hough Line detector + Morphological operations + finding non-linear places with simple kernel)
This method is not so easy to implement because it will require a lot of fine-tuning job, but I'm sure it's possible to realize it :)

~~~~

# Install all requirements
pip3 install -r requirements.txt 

# To run predictor:
python3 predict.py -p  path/to/images

# To run predictor and see detected dents:
python3 predict.py -p  path/to/images -s
~~~~